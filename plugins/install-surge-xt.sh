#!/usr/bin/env bash

set -e
set -u

URL="https://github.com/surge-synthesizer/releases-xt/releases/download/1.3.0/"
FILE="surge-xt-linux-x86_64-1.3.0.tar.gz"
SHA256="b862bda032f4a99e83109b10ee35e01386a11339da416b375f35fa6df0332acb"
VSTDIR="${HOME}/.vst3/SurgeXT"
USERDIR="${HOME}/Surge XT"

if [ ! -f ${FILE} ]; then
	curl -ORL ${URL}${FILE}
	echo "${SHA256} ${FILE}" | sha256sum -c -
else
	echo "file ${FILE} exists, skipping download"
fi

if [ ! -d ${VSTDIR} ]; then
	TMPDIR=$(mktemp -d)
	tar -C ${TMPDIR} -xzvf ${FILE}
	mkdir -p ${VSTDIR}
	mv -i ${TMPDIR}/lib/vst3/Surge* ${VSTDIR}
	mkdir -p "${USERDIR}/Patches/"
	mv -i ${TMPDIR}/share/surge-xt/patches* "${USERDIR}/Patches/"
	mkdir -p "${USERDIR}/Wavetables/"
	mv -i ${TMPDIR}/share/surge-xt/wavetables* "${USERDIR}/Wavetables/"
	rm -rf ${TMPDIR}
else
	echo "${VSTDIR} exists, aborting installation"
	exit 1
fi
